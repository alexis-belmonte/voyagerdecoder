#!/usr/bin/env python
#
# coding: utf-8
# 
# Voyager Decoder written in Python 3.8.x
# Copyright 2020 Alexis BELMONTE
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished
# to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#

# We import the `importlib` module
import importlib

# We import the `gi` module that will allow us to verify and use the Gtk, Gdk &
# Pango toolkits
#gi = importlib.import_module("gi")
import gi
gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")

# We import the `resources` module & make it accessible to all child modules
try:
    resources = importlib.import_module("importlib.resources")
# Fallback for older version of `importlib`
except ImportError:
    resources = importlib.import_module("importlib_resources")

with resources.path(__package__, "layout") as layout_folder:
    layout_path = layout_folder
    
# We import the modules that we need globally
import enum, threading, numpy

# We import the modules defined in this folder
import voyager.ui
import voyager.audio
import voyager.analysis
