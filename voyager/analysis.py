#!/usr/bin/env python
#
# coding: utf-8
# 
# Voyager Decoder written in Python 3.8.x
# Copyright 2020 Alexis BELMONTE
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished
# to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
# 

# We import our `audio` module
import voyager.audio

# We import the GTK module
from gi.repository import Gtk

class SpectrumAnalyzer(voyager.audio.AudioStreamHandler):
    # Function callback used when receiving data from the specified device,
    # when applicable.
    def read_data(self, audio_stream: voyager.audio.AudioStream, input_data: bytes,
                  frame_count: int, status: voyager.audio.AudioBufferStatus):
        pass
    
    # Function callback used when writing data to the specified device, when
    # applicable.
    def write_data(self, audio_stream: voyager.audio.AudioStream, frame_count: int,
                   sample_format: voyager.audio.AudioStreamFormat,
                   status: voyager.audio.AudioBufferStatus) -> bytes:
        pass
    
    # Function callback used to control the status of the assigned stream
    def get_stream_control(self) -> voyager.audio.AudioStreamStatus:
        pass
    
